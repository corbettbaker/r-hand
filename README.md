About

R-Hand is a set of tools for creating customized Fedora 18 Remix images for Raspberry Pi, Revision B. The intention is to create a device that allows remote client site administration with minimal technical knowledge. 

Features include: 
-Automatic reverse SSH tunnel configuration, run the included script to build an image with SSH keys, server configuration,etc. 
-Automatic network configuration of LAN, 3G Card, and others, just plug a device in, and R-Hand will attempt to reach the internet via any means available. 
-Automatic resizing of root filesystem to match Secure Digital card.
-Out of box support for configuring most Network devices using Serial, Ethernet, and Wireless.

Upcoming/Beta features:
-Ajenti integration.
-Automatic network scanning/configuration, provides links for RDP, VNC, and SSH machines available on 
-Kismet support with IDS configuration. 
