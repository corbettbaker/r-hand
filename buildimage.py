#!/usr/bin/python
import os, argparse
bootoffset=str(1048576)
rootoffset=str(54525952)
imagefile=str('rpfr-f18-final.img')
newimage=str('images/new.img')

parser = argparse.ArgumentParser(description='Mounts and modifies the R-Hand Image')
parser.add_argument('-i', metavar='<image.img>', type=str, help='Image Filename')

args = parser.parse_args()

from os import *
def mountimage():
    #Mount boot filesystem
    bootmount_command = 'mount -o loop,offset="%s" "%s" "%s"'% (bootoffset,imagefile, 'mount/boot')
    rootmount_command = 'mount -o loop,offset="%s" "%s" "%s"'% (rootoffset,imagefile, 'mount/root')
    system(bootmount_command)
    system(rootmount_command)
def unmountimage():
  bootunmount_command='umount mount/boot'
  rootunmount_command='umount mount/root'
  system(bootunmount_command)
  system(rootunmount_command)
  
def setboot():
  rename('mount/boot/config.txt','mount/boot/config.orig')
  system("cp %s %s" % ("templates/config.txt", "mount/boot/config.txt"))
  
def setroot():
  chdir('mount/root/lib/systemd/system')
  #Change default boot to multi-user to save memory.
  rename('default.target','default.old')
  symlink('mount/root/usr/lib/systemd/system/multi-user.target','default.target')
  #Remove initial configuration stage.
  chdir('../../../../')

mountimage()
setboot()
unmountimage()
