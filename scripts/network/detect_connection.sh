#!/bin/bash
PATH="/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin"
primary_gw="192.168.1.1" #for example.
check_one="8.8.8.8"
check_two="8.8.4.4"
#first we check internet connection.
if `ping -c 1 -W 1 $check_one |grep -E '(unreachable|100\%\ packet\ loss)' &> /dev/null` &&\
   `ping -c 1 -W 1 $check_two |grep -E '(unreachable|100\%\ packet\ loss)' &>/dev/null`
  then #if we don't have internet
    if [ -e /tmp/wan_backup ]
      #if we are using backup right now we try to change to primary connection
      then primary.sh && rm /tmp/wan_backup
      #else we change to wan backup.
      else backup.sh && touch /tmp/wan_backup
    fi
fi

#if we are using wan backup right now we check if primary connection works.
if [ -e /tmp/wan_backup ]
  then
    if `ip route add $check_one via $primary_gw; ip route add $check_two via $primary_gw;\
        sleep 2; ping -c 1 -W 1 $check_one | grep -E '(unreachable|100\%\ packet\ loss)' &> /dev/null &&\
        ping -c 1 -W 1 $check_two | grep -E '(unreachable|100\%\ packet\ loss)' &> /dev/null`
      then #don't works we clean the routes and stay using backup
        ip route del $check_one via $primary_gw
        ip route del $check_two via $primary_gw
      else #it works so we change active connection 
        ip route del $check_one via $primary_gw
        ip route del $check_two via $primary_gw
        primary.sh
        rm /tmp/wan_backup
    fi
fi
